import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArraysComponent } from './arrays.controller';
import { IRoute } from '@taedr/angular';

export const ARRAYS_ROUTE: IRoute = {
   path: `arrays`,
   component: ArraysComponent,
   children: [],
   data: { title: `Arrays` }
}



@NgModule({
   imports: [
      CommonModule,
   ],
   declarations: [
      ArraysComponent
   ],
   exports: [
      ArraysComponent
   ]
})
export class ArraysModule {

}




