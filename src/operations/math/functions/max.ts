import { ToNumber } from '@taedr/utils';

export interface IArrayMax<T> {
   readonly value: T;
   readonly index: number;
   readonly max: number;
}


export function max<T>(values: readonly T[], get: ToNumber<T>): IArrayMax<T> {
   let value: T;
   let index: number;
   let max = Number.NEGATIVE_INFINITY;
   
   for (let i = 0; i < values.length; i++) {
      const _value = values[i];
      const _max = get(_value);

      if (_max > max) {
         max = _max;
         value = _value;
         index = i;
      }
   }

   return { value, max, index };
}


