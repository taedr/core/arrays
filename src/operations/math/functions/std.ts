import { ToNumber } from '@taedr/utils';
import { avg } from './avg';


export function std<T>(values: readonly T[], get: ToNumber<T>) {
   const average = avg(values, get);
   const total = values.reduce((acc, val) => acc += (get(val) - average) ** 2, 0);
   return Math.sqrt(total / (values.length - 1));
}