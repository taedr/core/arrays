import { ToNumber } from '@taedr/utils';

export interface IArrayMin<T> {
   readonly value: T;
   readonly index: number;
   readonly min: number;
}


export function min<T>(values: readonly T[], get: ToNumber<T>): IArrayMin<T> {
   let value: T;
   let index: number;
   let min = Number.POSITIVE_INFINITY;

   for (let i = 0; i < values.length; i++) {
      const _value = values[i];
      const _min = get(_value);

      if (_min < min) {
         min = _min;
         value = _value;
         index = i;
      }
   }

   return { value, min, index };
}