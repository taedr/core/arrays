import { ToNumber } from '@taedr/utils';

export function avg<T>(values: readonly T[], get: ToNumber<T>): number {
   return values.reduce((acc, value) => acc += get(value), 0) / values.length;
}