import { TNew, IMap } from '@taedr/utils';


export function sliceByConstructor<T extends object>(
   values: T[], 
   builders: TNew<T>[]
): Map<TNew<T>, T[]> {
   const byContructorName: IMap<T[]> = {};
   const map = new Map<TNew<T>, T[]>();

   for (const builder of builders) {
      byContructorName[builder.name] = []
   }

   for (const value of values) {
      const name = value['constructor'].name;
      byContructorName[name].push(value);
   }

   for (const builder of builders) {
      const related = byContructorName[builder.name];
      map.set(builder, related);
   }

   return map;
}

