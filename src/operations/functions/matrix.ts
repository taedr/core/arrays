export type TMatrixProcessor<I, O> = (value: I, row: number, col: number) => O;


export function processMatrix<I, O>(matrix: I[][], processor: TMatrixProcessor<I, O>): O[] {
   const output: O[] = [];
   
   for (let i = 0; i < matrix.length; i++) {
      const row = matrix[i];
      for (let j = 0; j < row.length; j++) {
         const value = row[j];
         const processed = processor(value, i, j);
         output.push(processed);
      }
   }

   return output;
}