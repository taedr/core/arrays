import { ToNumber } from '@taedr/utils';

export type TSetter<T> = (val: T, index: number, newNum: number) => void;

export function balancer<T>(values: T[], min: number, get: ToNumber<T>, set: TSetter<T>) {
   const rootLength = values.length;
   let needToCmp = 0;
   let availableCmp = 0;

   for (let i = 0; i < rootLength; i++) {
      const num = get(values[i]);
      
      if (num <= min) {
         needToCmp += min - num;
      } else {
         availableCmp += num - min;
      }
   }

   const oneCmpPrc = needToCmp / 100;

   for (let i = 0; i < rootLength; i++) {
      const val = values[i];
      const num = get(val);
      
      if (num <= min) {
         set(val, i, num + (min - num))
      } else {
         const prc = (num - min) * 100 / availableCmp;
         set(val, i, num - (oneCmpPrc * prc));
      }
   }

}