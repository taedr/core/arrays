import { TMapper } from '@taedr/utils';

export type TMatrix<T> = T[][];

// ===============================================
//                Comparers
// ===============================================
/** Returns entries, which values prensents in all `arrays` */
export function correlation<T, V>(arrays: TMatrix<T>, by: TMapper<T, V>): T[] {
   const correlation: T[] = [];

   digger(arrays, by, (entry, presentIn, rowsCount) => {
      if (presentIn === rowsCount) {
         correlation.push(entry);
      }
   });

   return correlation;
}
/** Returns entries, which values prensents in two or more `arrays` */
export function correlationWeak<T, V>(arrays: TMatrix<T>, by: TMapper<T, V>): T[] {
   const correlation: T[] = [];

   digger(arrays, by, (entry, presentIn) => {
      if (presentIn > 1) {
         correlation.push(entry);
      }
   });

   return correlation;
}
/** Returns entrie, which values prensents only in one array */
export function difference<T, V>(arrays: TMatrix<T>, by: TMapper<T, V>): T[] {
   const difference: T[] = [];

   digger(arrays, by, (entry, presentIn) => {
      if (presentIn === 1) {
         difference.push(entry);
      }
   });

   return difference;
}
/** Returns entries, which values present not in all arrays */
export function differenceWeak<T, V>(arrays: TMatrix<T>, by: TMapper<T, V>): T[] {
   const difference: T[] = [];

   digger(arrays, by, (entry, presentIn, rowsCount) => {
      if (presentIn < rowsCount) {
         difference.push(entry);
      }
   });

   return difference;
}
/** Removes entries from root array, which values present in other arrays */
export function deduct<T, V>(root: T[], arrays: TMatrix<T>, by: TMapper<T, V>): T[] {
   const cleared: T[] = [];

   digger([root, ...arrays], by, (entry, presentIn) => {
      if (presentIn === 1) {
         cleared.push(entry);
      }
   }, 0, 1);

   return cleared;
}
/** Returns all entries with unique values from all arrays */
export function combine<T, V>(arrays: TMatrix<T>, by: TMapper<T, V>): T[] {
   const composition: T[] = [];

   digger(arrays, by, (entry) => {
      composition.push(entry);
   });

   return composition;
}
// ===============================================
//                Digger
// ===============================================
type THandler<T> = (entry: T, presentIn: number, rowsCount: number) => void;

function digger<T, V>(
   arrays: TMatrix<T>, 
   by: TMapper<T, V>, 
   handler: THandler<T>, 
   start?: number, 
   end?: number
) {
   if (arrays.length === 0) {
      return null;
   } else if (arrays.length === 1) {
      arrays[0].forEach(col => handler(col, 1, 1));
      return null;
   }

   const withData = arrays.filter(arr => arr.length > 0);
   const colsCountByRow = withData.map(arr => arr.length);
   const rowsCount = withData.length;
   const startFrom = start || 0;
   const endOn = end || rowsCount;
   const processed = new Set<V>();

   for (let row = startFrom; row < endOn; row++) {
      const startFromNext = row + 1;

      for (let col = 0; col < colsCountByRow[row]; col++) {
         const entry = withData[row][col];
         const value = by(entry);
         let presentIn = 1;

         if (processed.has(value)) {  //* Skip duplicate entry 
            continue;
         } else {
            processed.add(value);
         }

         for (let i = startFromNext; i < rowsCount; i++) {
            let isMatch = false;

            for (let j = 0; j < colsCountByRow[i]; j++) {
               const _value = by(withData[i][j]);
               isMatch = value === _value;

               if (isMatch) {
                  presentIn += 1;
                  break;
               }
            }
         }

         handler(entry, presentIn, rowsCount);
      }
   }
}

/*
[1, 1, 2, 3, 4, 5, 6, 7, 8];
[2, 8, 9, 11 , 5 , 6];
[32, 11, 5, 11, 1, 1 6, 12];
[15, 17, 19, 7, 6, 11]


*/