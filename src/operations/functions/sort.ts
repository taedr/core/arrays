import { IMap, IS, TSorter, ToNumber, ToString } from '@taedr/utils';


export type TSortState = 'asc' | 'desc';
export type TSortBy<T> = ToNumber<T> | ToString<T>;

export interface ISortable<T> {
   readonly by: TSortBy<T>;
   readonly state: TSortState;
}


export const SORT = Object.freeze({
   string<T>(state: TSortState, by: ToString<T>) {
      if (state === `asc`) return (a: T, b: T) => by(a).localeCompare(by(b));
      if (state === `desc`) return (a: T, b: T) => by(b).localeCompare(by(a));
   },
   number<T>(state: TSortState, by: ToNumber<T>) {
      if (state === `asc`) return (a: T, b: T) => by(a) - by(b);
      if (state === `desc`) return (a: T, b: T) => by(b) - by(a);
   }
});


export function _sort<T>(array: T[], state: TSortState, by: TSortBy<T>): T[];
export function _sort<T>(array: T[], chain: ISortable<T>[]): T[];
export function _sort<T>(array: T[], b: TSortState | ISortable<T>[], c?: TSortBy<T>): T[] {
   const chain = IS.array(b) ? b : [{ by: c, state: b }];
   if (array.length <= 1 || chain.length === 0) return array; // #Return#

   const [first] = array;
   const nodes: ISortNode<T>[] = chain.map(({ by, state }) => {
      const val = by(first);
      let sorter: TSorter<T>

      if (IS.number(val)) sorter = SORT.number(state, by as ToNumber<T>);
      if (IS.string(val)) sorter = SORT.string(state, by as ToString<T>);

      return { by, sorter };
   });

   return _sortByChain(array, nodes);
}


function _sortByChain<T>(array: T[], chain: ISortNode<T>[]): T[] {
   const groups: IMap<T[]> = {};
   const sortedGroups: T[] = [];
   const [sortable, ...tail] = chain;

   for (const value of array) {
      const sortBy = sortable.by(value) + '';
      groups[sortBy] = groups[sortBy] || [];
      groups[sortBy].push(value);
   }

   for (const id in groups) {
      const group = groups[id];
      let sortedGroup = group;

      if (group.length > 1 && tail.length >= 1) {
         sortedGroup = _sortByChain(group, tail);
      }

      sortedGroups.push(...sortedGroup);
   }

   sortedGroups.sort(sortable.sorter);
   return sortedGroups;
}


export function getNextSortState(state: TSortState): TSortState {
   switch (state) {
      case 'desc': return 'asc';
      case 'asc': return 'desc';
   }
}



interface ISortNode<T> {
   readonly by: ToNumber<T> | ToString<T>;
   readonly sorter: TSorter<T>;
}