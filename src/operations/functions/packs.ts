import { TFMorph, getMorph } from '@taedr/utils';

export function sliceOnPacks<T>(array: T[], size: number): T[][] {
   const packsCount = Math.ceil(array.length / size);
   const sliced: T[][] = [];

   for (let i = 0; i < packsCount; i++) {
      const start = i * size;
      const end = start + size;

      sliced.push(array.slice(start, end));
   }

   return sliced;
}


export async function processByPack<I, O>(
   values: I[], 
   packSize: number, 
   processor: TFMorph<I[], O[]>
): Promise<O[]> {
   const packs = sliceOnPacks(values, packSize);
   const promises = packs.map(pack => new Promise<O[]>(resolve => {
      setTimeout(async () => {
         const result = await getMorph(processor, pack);
         resolve(result);
      });
   }));
   const result = await Promise.all(promises);
   const merged = result.reduce((acc, pack) => acc.concat(pack), []);

   return merged;
}