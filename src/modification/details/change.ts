export interface IChange<T> {
   readonly value: T;
   readonly index: number;
}