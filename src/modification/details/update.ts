import { IChange } from './change';


export interface IUpdate<T> extends IChange<T> {
   readonly prev: T;
}


export interface IUpdates<T> {
   readonly changes: readonly IUpdate<T>[];
   readonly notMatched: readonly T[];
}
