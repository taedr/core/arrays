/** Mark showing that actions should be performed with in START of the entity.  */
export const HEAD = Symbol(`HEAD`);
/** Mark showing that actions should be performed with in END of the entity.  */
export const TAIL = Symbol(`TAIL`);

export type TPosition = typeof HEAD | typeof TAIL | number;

export const ADDED = Symbol(`ADDED`);
export const UPDATED = Symbol(`UPDATED`);
export const DUPLICATE = Symbol(`DUPLICATE`);

export type TMerge = typeof ADDED | typeof UPDATED | typeof DUPLICATE; 