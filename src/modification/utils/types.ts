import { ToUnknown, TMapper } from '@taedr/utils';

export type TKey<T> = ToUnknown<T>;
export type TBy<I, O> = TMapper<I, O>;
export type TMatch<T> = (value: T, index: number) => boolean;