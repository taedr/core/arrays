import { TProducer, TConsumer } from '@taedr/utils';
import { IChange } from '../../details/change';
import { insertPosition } from './functions/position';
import { TAIL, HEAD, TPosition } from '../../utils/symbols';
import { insertByIndexes } from './functions/indexes';

// ==================================================
//                   Constructor
// ==================================================
export function _insert<T>(array: T[]): Inserter<T> {
   return new Inserter(() => array);
}
// ==================================================
//                   Class
// ==================================================
export class Inserter<T> {
   constructor(
      protected readonly _getArray: TProducer<T[]>,
      protected readonly _onInsert?: TConsumer<IChange<T>[]>,
   ) { }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   public tail(...values: T[]) {
      return this.insert(values, TAIL);
   }


   public head(...values: T[]) {
      return this.insert(values, HEAD);
   }


   public index(index: number, ...values: T[]) {
      return this.insert(values, index);
   }


   public indexes(...values: IChange<T>[]) {
      const changes = insertByIndexes(this._getArray(), values);
      this.event(changes);
      return changes;
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected insert(values: T[], position: TPosition) {
      const changes = insertPosition(this._getArray(), values, position);
      this.event(changes);
      return changes;
   }


   protected event(changes: IChange<T>[]) {
      if (this._onInsert && changes.length) {
         this._onInsert(changes);
      }
   }
}