import { IChange } from '../../../details/change';
import { deleteDuplicates, _delete } from '../../delete';

export function insertByIndexes<T>(
   array: T[],
   values: IChange<T>[],
): IChange<T>[] {
   deleteDuplicates(values, ({ index }) => index);
   const changes = [...values].sort((a, b) => a.index - b.index);

   for (let i = 0; i < changes.length; i++) {
      const { index, value } = changes[i];
      array.splice(index, 0, value);
   }

   return changes;
}