import { IChange } from '../../../details/change';
import { TPosition, TAIL, HEAD } from '../../../utils/symbols';

export function insertPosition<T>(
   array: T[],
   values: ReadonlyArray<T>,
   position: TPosition,
): IChange<T>[] {
   let startIndex: number;

   if (position === TAIL) {
      startIndex = array.length;
      array.push(...values);
   } else if (position === HEAD) {
      startIndex = 0;
      array.unshift(...values);
   } else {
      startIndex = position;
      array.splice(position, 0, ...values);
   }

   const changes: IChange<T>[] = values.map((value, i) => ({ value, index: i + startIndex }));

   return changes;
}