import { FN_PACK, TProducer, IS, TConsumer } from '@taedr/utils';
import { deleteDuplicates } from './functions/duplicates';
import { deleteIndexes } from './functions/indexes';
import { deleteValues } from './functions/values';
import { deleteMatches } from './functions/match';
import { deleteCrosses } from './functions/cross';
import { deletePosition } from './functions/position';
import { IChange } from '../../details/change';
import { TKey, TMatch, TBy } from '../../utils/types';
import { HEAD, TAIL } from '../../utils/symbols';

// ==================================================
//                   Constructor
// ==================================================
export function _delete<T>(array: T[]): Deleter<T> {
   return new Deleter(() => array);
}
// ==================================================
//                   Class
// ==================================================
export class Deleter<T>  {
   constructor(
      protected readonly _getArray: TProducer<T[]>,
      protected readonly _onDelete?: (changes: IChange<T>[], array: T[]) => void,
   ) { }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   public all() {
      const indexes = FN_PACK(this._getArray().length);
      return this.index(indexes);
   }


   public duplicates(key: TKey<T>) {
      const array = this._getArray();
      const changes = deleteDuplicates(array, key);
      this.event(changes, array);
      return changes;
   }


   public match(by: TMatch<T>) {
      const array = this._getArray();
      const changes = deleteMatches(array, by);
      this.event(changes, array);
      return changes;
   }


   public cross<V, K>(key: TBy<T | V, K>, ...values: V[]) {
      const array = this._getArray();
      const changes = deleteCrosses(array, values, key);
      this.event(changes, array);
      return changes;
   }


   public values(...values: T[]) {
      const array = this._getArray();
      const changes = deleteValues(array, values);
      this.event(changes, array);
      return changes;
   }


   public index(index: number): T
   public index(indexes: number[]): IChange<T>[]
   public index(indexes: number | number[]): T | IChange<T>[] {
      const array = this._getArray();
      const _indexes = IS.array(indexes) ? indexes : [indexes];
      const changes = deleteIndexes(array, _indexes);
      this.event(changes, array);
      return IS.array(indexes) ? changes : changes[0]?.value;
   }


   public head(): T
   public head(count: number): IChange<T>[];
   public head(count?: number) {
      const array = this._getArray();
      const changes = deletePosition(array, HEAD, count);
      this.event(changes, array);
      return IS.number(count) ? changes : changes[0]?.value;
   }


   public tail(): T
   public tail(count: number): IChange<T>[];
   public tail(count?: number) {
      const array = this._getArray();
      const changes = deletePosition(array, TAIL, count);
      this.event(changes, array);
      return IS.number(count) ? changes : changes[0]?.value;
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected event(changes: IChange<T>[], array: T[]) {
      if (this._onDelete && changes.length) {
         this._onDelete([...changes], array);
      }
   }
}