import { IS } from '@taedr/utils';
import { IChange } from '../../../details/change';
import { TPosition, HEAD, TAIL } from '../../../utils/symbols';

 

export function deletePosition<T>(
   array: T[],
   position: TPosition,
   count = 1
): IChange<T>[] {
   let index: number;

   if (IS.number(position)) {
      index = position;
   } else if (position === HEAD) {
      index = 0;
   } if (position === TAIL) {
      index = array.length - count;
   }

   const changes: IChange<T>[] = [];
   const steps = Math.min(index + count, array.length)

   for (let i = index; i < steps; i++) {
      const value = array[i];
      changes.push({ value, index: i });
   }

   array.splice(index, count);

   return changes;
} 