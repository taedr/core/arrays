import { IChange } from '../../../details/change';
import { deleteIndexes } from './indexes';

export function deleteValues<T>(array: T[], values: T[]): IChange<T>[] {
   const indexes: number[] = [];
   const _values = new Set(values);

   for (let i = 0; i < array.length; i++) {
      if (_values.has(array[i])) {
         indexes.push(i)
      }
   }

   return deleteIndexes(array, indexes);
}


