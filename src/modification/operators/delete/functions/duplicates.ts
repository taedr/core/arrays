import { IS } from '@taedr/utils';
import { deleteIndexes } from './indexes';
import { IChange } from '../../../details/change';
import { TKey } from '../../../utils/types';


export function deleteDuplicates<T>(
   array: T[],
   key: TKey<T>
): IChange<T>[] {
   const latest = new Set<unknown>();
   const indexes: number[] = [];

   for (let i = array.length - 1; i >= 0; i--) {
      const value = array[i];
      if (IS.null(value)) continue; // #Continue#
      const compare = key(value);

      if (latest.has(compare)) {
         indexes.push(i);
      } else {
         if (!IS.null(compare)) latest.add(compare);
      }
   }

   return deleteIndexes(array, indexes);
}