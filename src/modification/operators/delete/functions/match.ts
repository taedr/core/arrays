import { deleteIndexes } from './indexes';
import { IChange } from '../../../details/change';
import { TMatch } from '../../../utils/types';


export function deleteMatches<T>(array: T[], by: TMatch<T>): IChange<T>[] {
   const indexes: number[] = [];

   for (let i = 0; i < array.length; i++) {
      const value = array[i];
      if (by(value, i)) {
         indexes.push(i);
      }
   }

   return deleteIndexes(array, indexes);
}

