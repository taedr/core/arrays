import { deleteIndexes } from './indexes';
import { IChange } from '../../../details/change';
import { TBy } from '../../../utils/types';


export function deleteCrosses<T, V, K>(
   array: T[],
   values: V[],
   key: TBy<T | V, K>
): IChange<T>[] {
   const indexes: number[] = [];
   const commons = new Set<K>();

   for (let i = 0; i < values.length; i++) {
      const value = values[i];
      const byValue = key(value);
      commons.add(byValue);
   }


   for (let i = 0; i < array.length; i++) {
      const value = array[i];
      const common = key(value);

      if (commons.has(common)) {
         indexes.push(i);
      }
   }

   return deleteIndexes(array, indexes);
}