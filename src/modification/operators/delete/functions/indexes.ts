import { IChange } from '../../../details/change';

export function deleteIndexes<T>(array: T[], _indexes: number[]): IChange<T>[] {
   const size = array.length;
   const bounded = _indexes.filter(i => i >= 0 && i < size);
   const unique = new Set(bounded);
   const indexes = Array.from(unique).sort();
   const changes: IChange<T>[] = indexes.map(i => ({ value: array[i], index: i }));

   if (size === 0 || indexes.length === 0) return []; // #Return#
   if (indexes.length === size) { // #Return#
      array.length = 0;
      return changes;
   }

   let nextDelete = indexes.shift();
   let back = 0;

   for (let i = nextDelete; i < size; i++) {
      if (i === nextDelete) {
         back += 1;
         nextDelete = indexes.shift();
      } else {
         array[i - back] = array[i];
      }
   }

   array.length -= back;

   return changes;
}