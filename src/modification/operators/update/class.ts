import { updateByKey } from './functions/key';
import { updateByMap } from './functions/map';
import { TProducer, TConsumer } from '@taedr/utils';
import { IUpdates } from '../../details/update';
import { updateByIndexes } from './functions/indexes';
import { IChange } from '../../details/change';
import { TKey } from '../../utils/types';

// ==================================================
//                   Factory
// ==================================================
export function _update<T>(array: T[]): Readonly<Updater<T>> {
   return new Updater(() => array);
}
// ==================================================
//                   Class
// ==================================================
export class Updater<T> {
   constructor(
      protected readonly _getArray: TProducer<T[]>,
      protected readonly _onUpdate?: TConsumer<IUpdates<T>>,
   ) { }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   public indexes(...values: IChange<T>[]) {
      const updates = updateByIndexes(this._getArray(), values);
      this.event(updates);
      return updates
   }


   public keys(key: TKey<T>, ...values: T[]): IUpdates<T>;
   public keys(key: TKey<T>, map: Map<unknown, number>, ...values: T[]): IUpdates<T>;
   public keys(key: TKey<T>, first: T | Map<unknown, number>, ...other: T[]): IUpdates<T> {
      const storage = this._getArray();
      let updates: IUpdates<T>;

      if (first instanceof Map) {
         updates = updateByMap(storage, other, key, first);
      } else {
         updates = updateByKey(storage, [first, ...other], key);
      }

      this.event(updates);

      return updates;
   }
   // ---------------------------------------------
   //                   Internal
   // ---------------------------------------------
   protected event(changes: IUpdates<T>) {
      if (this._onUpdate && changes.changes.length) {
         this._onUpdate(changes);
      }
   }
}