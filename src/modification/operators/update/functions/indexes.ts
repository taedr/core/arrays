import { IS } from '@taedr/utils';
import { IChange } from '../../../details/change';
import { deleteDuplicates, _delete } from '../../delete';
import { IUpdates, IUpdate } from '../../../details/update';

export function updateByIndexes<T>(array: T[], values: IChange<T>[]): IUpdates<T> {
   deleteDuplicates(values, ({ index }) => index);

   const changes: IUpdate<T>[] = [];
   const notMatched: T[] = [];

   for (let i = 0; i < values.length; i++) {
      const { index, value } = values[i];
      const prev = array[index];

      if (IS.null(array[index])) { // #Continue#
         notMatched.push(value);
         continue;
      }

      array[index] = value;
      changes.push(({
         value,
         prev,
         index,
      }));
   }

   return { changes, notMatched };
}
