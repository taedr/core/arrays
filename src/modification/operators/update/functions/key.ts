import { IS, FN_NULL } from '@taedr/utils';
import { IUpdates, IUpdate } from '../../../details/update';
import { TKey } from '../../../utils/types';

export function updateByKey<T>(array: T[], values: Iterable<T>, key: TKey<T>): IUpdates<T> {
   if (key === FN_NULL) return { changes: [], notMatched: [...values] }; // #Return#

   const changes: IUpdate<T>[] = [];
   const notMatched: T[] = [];
   const _values = new Map<unknown, T>();

   for (const _value of values) {
      const value = _value ?? null;

      if (value === null) continue; // #Continue#
      const valueKey = key(value) ?? null;

      if (valueKey === null) {
         notMatched.push(value);
      } else {
         _values.set(valueKey, value);
      }
   }


   for (let i = 0; i < array.length; i++) {
      const prev = array[i] ?? null;
      const prevKey = prev === null ? null : key(prev) ?? null;
      const value = _values.get(prevKey) ?? null;

      if (prevKey === null || value === null) continue; // #continue#

      array[i] = value;
      changes.push({
         value,
         prev,
         index: i,
      });
      _values.delete(prevKey);
   }

   notMatched.push(..._values.values());

   return { changes, notMatched };
}
