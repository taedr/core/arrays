import { IUpdates, IUpdate } from '../../../details/update';
import { IS } from '@taedr/utils';
import { TKey } from '../../../utils/types';

export function updateByMap<T>(
   array: T[],
   values: readonly T[],
   key: TKey<T>,
   map: Map<unknown, number>
): IUpdates<T> {
   const changes: IUpdate<T>[] = [];
   const notMatched: T[] = [];
   const used = new Set<unknown>();

   for (let i = 0; i < values.length; i++) {
      const value = values[i] ?? null;

      if (value === null) continue; //#Continue#

      const valueBy = key(value) ?? null;
      const index = map.get(valueBy);
      const prev = array[index] ?? null;

      if (valueBy === null) {
         notMatched.push(value);
      } else if (prev === null) {
         if (used.has(valueBy)) continue; // #Continue#
         used.add(valueBy);
         notMatched.push(value);
      } else {
         array[index] = value;
         changes.push({
            value,
            prev,
            index,
         });
      }
   }

   changes.sort((a, b) => a.index - b.index);

   return { changes, notMatched };
}
