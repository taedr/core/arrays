import { TMapper } from '@taedr/utils';
import { ITag } from '../interfaces/tag';
import { AFilter } from './core';
// ==================================================
//                      Tag
// ==================================================
export interface IBooleanTag extends ITag<boolean> {}

// ==================================================
//                   Filter
// ==================================================
export class BooleanFilter extends AFilter<boolean, IBooleanTag> {
   protected getMatcher<T>(tag: IBooleanTag, get: TMapper<T, boolean>) {
      return (val: T) => get(val) === tag.value;
   }
}

export const BOOLEAN_FILTER = new BooleanFilter();
