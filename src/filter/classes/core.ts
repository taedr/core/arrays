import { TMapper } from '@taedr/utils';
import { ITag, TTagsMode } from '../interfaces/tag';
import { TMatch } from '../../modification';
// =======================================================
//                      Class
// =======================================================
export abstract class AFilter<V, F extends ITag<V>> {
   // ----------------------------------------------------
   //                   Abstract
   // ----------------------------------------------------
   protected abstract getMatcher<T>(tag: F, by: TMapper<T, V>): TMatch<T>;
   // ----------------------------------------------------
   //                   Api
   // ----------------------------------------------------
   public tag<T>(tag: F, values: ReadonlyArray<T>, by: TMapper<T, V>): T[] {
      const [comparer] = this.getMatchers([tag], by);
      return values.filter(comparer);
   }


   public group<T>(tags: ReadonlyArray<F>, mode: TTagsMode, values: ReadonlyArray<T>, by: TMapper<T, V>): T[] {
      if (tags.length === 0 || values.length <= 1) return [...values];
      const comparers = this.getMatchers(tags, by);

      switch (mode) {
         case 'AND': return values.filter((value, i) => {
            return comparers.every(matcher => matcher(value, i));
         });
         case 'OR': return values.filter((value, i) => {
            return comparers.some(matcher => matcher(value, i));
         });
         default: throw new Error(`Unknown Tag Group mode: ${mode}`);
      }
   }
   // ----------------------------------------------------
   //                      Internal
   // ----------------------------------------------------
   private getMatchers<T>(tags: ReadonlyArray<F>, by: TMapper<T, V>): TMatch<T>[] {
      return tags.map(tag => {
         const comparer = this.getMatcher(tag, by);

         if (tag.result === 'EXCLUDE') {
            return (value: T, index: number) => !comparer(value, index);
         } else {
            return comparer;
         }
      });
   }
}

