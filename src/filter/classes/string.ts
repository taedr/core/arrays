import { ITag } from '../interfaces/tag';
import { ToBoolean, TMapper } from '@taedr/utils';
import { AFilter } from './core';
// ==================================================
//                   Tag
// ==================================================
export type TStringTagMode = 'START' | 'END' | 'CONTAINS' | 'EQUALS';
export type TStringTagCase = 'LOWER' | 'UPPER' | 'IGNORE' | 'INITIAL';


export interface IStringTag extends ITag<string> {
   readonly mode: TStringTagMode;
}
// ==================================================
//                   Filter
// ==================================================
export class StringFilter extends AFilter<string, IStringTag> {
   protected getMatcher<T>({ mode, value }: IStringTag, getString: TMapper<T, string>) {
      let comparer: ToBoolean<T>;
      let string = value.toLowerCase();
      let get = (val: T) => getString(val).toLowerCase();;

      const length = string.length;

      switch (mode) {
         case 'CONTAINS': comparer = val => get(val).includes(string); break;
         case 'START': comparer = val => get(val).slice(0, length) === string; break;
         case 'END': comparer = val => get(val).slice(-length) === string; break;
         case 'EQUALS': comparer = val => get(val) === string; break;
         default: throw new Error(`Unknown String Tag mode: ${mode}`);
      }

      return comparer;
   }
}



export const STRING_FILTER = new StringFilter();