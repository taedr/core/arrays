import { ITag } from '../interfaces/tag';
import { TMapper, ToBoolean } from '@taedr/utils';
import { AFilter } from './core';
// ==================================================
//                      Tag
// ==================================================
export type TNumberTagMode = '=' | '<' | '>' | '>=' | '<=';

export interface INumberTag extends ITag<number> {
   readonly mode: TNumberTagMode;
}
// ==================================================
//                   Filter
// ==================================================
export class NumberFilter extends AFilter<number, INumberTag> {
   protected getMatcher<T>(tag: INumberTag, converter: TMapper<T, number>) {
      const number = tag.value;
      let comparer: ToBoolean<T>;

      switch (tag.mode) {
         case '=': comparer = val => converter(val) === number; break;
         case '<': comparer = val => converter(val) < number; break;
         case '>': comparer = val => converter(val) > number; break;
         case '>=': comparer = val => converter(val) >= number; break;
         case '<=': comparer = val => converter(val) <= number; break;
         default: throw new Error(`Unknown Number Tag mode: ${tag.mode}`);
      }

      return comparer;
   }
}

export const NUMBER_FILTER = new NumberFilter();