import { ITag } from '../interfaces/tag';
import { TMapper, IS } from '@taedr/utils';
import { AFilter } from './core';
// ================================================== 
//                    Tag
// ==================================================
export interface IObjectTag<T extends object> extends ITag<T> {}

// ==================================================
//                   Filter
// ==================================================
export class ObjectFilter<O extends object> extends AFilter<O, IObjectTag<O>> {
   protected getMatcher<I>(tag: IObjectTag<O>, mapper: TMapper<I, O>) {
      return (val: I) => IS.equal.json(mapper(val), tag.value);
   }
}

export const OBJECT_FILTER = new ObjectFilter();