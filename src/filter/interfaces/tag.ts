export type TTagsMode = 'AND' | 'OR';

export type TTagResultMode = 'INCLUDE' | 'EXCLUDE';

export interface ITag<T> {
   readonly value: T;
   readonly result?: TTagResultMode;
}
