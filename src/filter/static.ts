import { STRING_FILTER } from './classes/string';
import { NUMBER_FILTER } from './classes/number';
import { BOOLEAN_FILTER } from './classes/boolean';
import { OBJECT_FILTER } from './classes/object';

export const FILTER = Object.freeze({
   string: STRING_FILTER,
   number: NUMBER_FILTER,
   boolean: BOOLEAN_FILTER,
   object: OBJECT_FILTER,
});