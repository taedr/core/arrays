import { IS } from '@taedr/utils';
import { IChange, IUpdate, IUpdates, TPosition, HEAD, TAIL } from '../modification';


export const ARRAYS_IS = Object.freeze({
   change(value: unknown): value is IChange<unknown> {
      return IS.object(value) && [`value`, `index`].every(key => key in value);
   },
   changes(value: unknown): value is IChange<unknown> {
      return IS.array(value) && ARRAYS_IS.change(value[0]);
   },
   update(value: unknown): value is IUpdate<unknown> {
      return ARRAYS_IS.change(value) && [`prev`].every(key => key in value);
   },
   updates(value: unknown): value is IUpdates<unknown> {
      return IS.object(value) && [`updated`, `notMatched`].every(key => key in value);
   },
   side(value: unknown): value is TPosition {
      return value === HEAD || value === TAIL || IS.number(value);
   }
});
