import { iterator } from './iterator';

export class Structure<T> {
   protected _size = 0;

   public get size() { return this._size; }


   constructor(values?: Iterable<T>) {
      for (const value of values ?? []) {
         this.add(value);
      }
   }
   // ---------------------------------------------
   //                   Api
   // ---------------------------------------------
   public add(value: T) {
      const index = this.indexOf(value) ?? ++this._size - 1;
      this[index] = value;
   }


   public delete(value: T) {
      const index = this.indexOf(value);
      if (index === undefined) return; // #Return#

      for (let i = index + 1; i < this._size; i++) {
         this[i - 1] = this[i];
      }

      this._size -= 1;
      delete this[this._size];
   }


   public indexOf(value: T) {
      for (let i = 0; i < this._size; i++) {
         if (this[i] === value) return i;
      }
   }


   readonly [Symbol.iterator] = iterator({
      next: index => this[index]
   });
}