import { IS, TConsumer } from '@taedr/utils';



export function getIterableSize(iterable: Iterable<unknown>): number {
   const iterator = iterable[Symbol.iterator]();
   let size = 0;
   while(iterator.next().done === false) size++;
   return size;
}


export function goThroughIterable<T>(iterable: Iterable<T>, fn: TConsumer<T>) {
   const iterator = iterable[Symbol.iterator]();
   let next = iterator.next();


   while(next.done === false) {
      fn(next.value);
      next = iterator.next();
   }
}
