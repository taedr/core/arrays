import { Structure } from '@taedr/arrays';


describe(Structure.name, () => {
   it(``, async () => {
      const structure = new Structure([1, 2, 1, 3, 2]);

      expect([...structure]).toEqual([1, 2, 3]);

      structure.add(3);
      structure.add(5);
      structure.add(6);
      structure.add(1);

      expect([...structure]).toEqual([1, 2, 3, 5, 6]);

      structure.delete(1);
      structure.delete(2);
      structure.delete(6);

      expect([...structure]).toEqual([3, 5]);

      structure //?
   });
});