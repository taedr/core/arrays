import { iterator, getIterableSize, goThroughIterable } from '@taedr/arrays';


describe(iterator.name, () => {
   class Iterable {
      readonly from = 0;
      readonly to = 10;
      public stopedOn: number;

      public [Symbol.iterator] = iterator({
         next: i => i < this.to ? i : null,
         onEnd: i => this.stopedOn = i,
      });
   }

   it(`Iterable`, async () => {
      let index = 0;
      const iterable = new Iterable();
      const stopOn = 8;

      for (const val of iterable) {
         expect(val).toEqual(index++);
         if (index === stopOn) break;
      }

      expect(iterable.stopedOn).toBe(stopOn);
   });

   it(`Size`, async () => {
      const iterable = new Iterable();
      const size = getIterableSize(iterable);

      expect(size).toEqual(iterable.to);
      expect(iterable.stopedOn).toBe(10);
   });


   it(`Go`, async () => {
      const iterable = new Iterable();
      let index = 0;

      goThroughIterable(iterable, num => {
         expect(num).toEqual(index++);
      });

      expect(iterable.stopedOn).toBe(10);
   });
});