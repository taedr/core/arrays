import { HEAD, TAIL, ARRAYS_IS, IChange, IUpdate, IUpdates } from '@taedr/arrays';

describe(`Typeof`, () => {
   const indexed = { index: 0 };
   const change: IChange<number> = { index: 0, value: 0 };
   const update: IUpdate<number> = { index: 0, value: 0, prev: 0}
   const updates: IUpdates<number> = { notMatched: [], updated: [] }


   it(`Change`, () => {
      expect(ARRAYS_IS.change(change)).toBeTruthy();
      expect(ARRAYS_IS.change(update)).toBeTruthy();

      expect(ARRAYS_IS.change(indexed)).toBeFalsy();
   });

   it(`Changes`, () => {
      expect(ARRAYS_IS.changes([change])).toBeTruthy();

      expect(ARRAYS_IS.changes(update)).toBeFalsy();
      expect(ARRAYS_IS.change(indexed)).toBeFalsy();
   });

   it(`Update`, () => {
      expect(ARRAYS_IS.update(update)).toBeTruthy();

      expect(ARRAYS_IS.update(change as any)).toBeFalsy();
      expect(ARRAYS_IS.update(indexed as any)).toBeFalsy();
   });

   it(`Updates`, () => {
      expect(ARRAYS_IS.updates(updates)).toBeTruthy();

      expect(ARRAYS_IS.update(change as any)).toBeFalsy();
      expect(ARRAYS_IS.update(indexed as any)).toBeFalsy();
   });

   it(`Side`, () => {
      expect(ARRAYS_IS.side(HEAD)).toBeTruthy();
      expect(ARRAYS_IS.side(TAIL)).toBeTruthy();
      expect(ARRAYS_IS.side(0)).toBeTruthy();
   });
});
