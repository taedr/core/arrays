import { FN_NULL, FN_RANDOM_NUM, FN_PACK, IMap, IS, TNew, TConsumer, performanceMs, } from '@taedr/utils';
import { updateByKey } from '@taedr/arrays';


describe(`Playground`, () => {
   it(``, async () => {
      const size = 100000;
      const values = FN_PACK(size);

      let mark = 0;

      const _for = performanceMs(() => {
         const len = values.length
         for (let i = 0; i < len; i++) {
            const value = values[i];
            if (value % 2 === 0) mark += 1;
         }
      })

      const _forOf = performanceMs(() => {
         for (const value of values) {
            if (value % 2 === 0) mark += 1;
         }
      })

      const _forIn = performanceMs(() => {
         for (const index in values) {
            const value = values[index];
            if (value % 2 === 0) mark += 1;
         }
      })

      const _forEach = performanceMs(() => {
         values.forEach(value => {
            if (value % 2 === 0) mark += 1;
         })
      })
   
      mark //?

      _for.duration     //?
      _forOf.duration   //?
      _forIn.duration   //?
      _forEach.duration //?

      


      expect(true).toEqual(true);
   });



});