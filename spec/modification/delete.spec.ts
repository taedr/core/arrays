import { _delete } from '@taedr/arrays';


describe(`Delete`, () => {
   class User {
      constructor(
         readonly id: string
      ) { }
   }

   const users = [
      new User(`Vasya`),
      new User(`Petya`),
      new User(`Ibragim`),
      new User(`Afanasii`),
      new User(`Igor`),
      new User(`Katya`),
      new User(`Ipolit`),
   ];
   const indexes: number[] = [1, 3, 5];
   const toDelete = indexes.map(i => users[i]);
   const Evalues = users.filter((_, i) => !indexes.includes(i));
   const Edeleted = indexes.map(i => ({ value: users[i], index: i }));


   it(`All`, async () => {
      const values = [...users];

      const Evalues = [];
      const Edeleted = values.map((value, index) => ({ value, index }));

      const deleted = _delete(values).all();

      values   //?
      Evalues  //?
      deleted  //?
      Edeleted //?

      expect(values).toEqual(Evalues);
      expect(deleted).toEqual(Edeleted);
   });


   it(`Cross`, async () => {
      const values = [...users];

      const deleted = _delete(values).cross(user => user.id, ...toDelete);

      values   //?
      Evalues  //?
      deleted  //?
      Edeleted //?

      expect(values).toEqual(Evalues);
      expect(deleted).toEqual(Edeleted);
   });


   it(`By values`, async () => {
      const values = [...users];

      const deleted = _delete(values).values(...toDelete);

      values   //?
      Evalues   //?
      deleted  //?
      Edeleted //?

      expect(values).toEqual(Evalues);
      expect(deleted).toEqual(Edeleted);
   });

   it(`By indexes`, async () => {
      const values = [...users];

      const deleted = _delete(values).index(indexes);

      values   //?
      Evalues   //?
      deleted  //?
      Edeleted //?

      expect(values).toEqual(Evalues);
      expect(deleted).toEqual(Edeleted);
   });


   it(`Match`, async () => {
      const values = [...users];

      const deleted = _delete(values).match((_, i) => indexes.includes(i));


      values   //?
      Evalues   //?
      deleted  //?
      Edeleted //?

      expect(values).toEqual(Evalues);
      expect(deleted).toEqual(Edeleted);
   });


   it(`HEAD`, async () => {
      const values = [...users];
      const count = 3;
      const Evalues = users.slice(count);
      const Edeleted = users.slice(0, count).map((value, index) => ({ value, index }));

      const deleted = _delete(values).head(count);

      values   //?
      Evalues  //?
      deleted  //?
      Edeleted //?

      expect(values).toEqual(Evalues);
      expect(deleted).toEqual(Edeleted);
   });


   it(`TAIL`, async () => {
      const values = [...users];
      const count = 3;
      const Evalues = users.slice(0, -count);
      const startIndex = users.length - count;
      const Edeleted = users.slice(-count).map((value, i) => ({ value, index: startIndex + i }));

      const deleted = _delete(values).tail(count);

      values   //?
      Evalues  //?
      deleted  //?
      Edeleted //?

      expect(values).toEqual(Evalues);
      expect(deleted).toEqual(Edeleted);
   });




   it(`Duplicates`, async () => {
      const empty = [new User(null), new User(undefined), new User(null)];
      const values = [...users, ...empty, null, ...users, null];
      const Eusers = [...empty, null, ...users, null];
      const Edeleted = users.map((value, index) => ({ value, index }));

      const deleted = _delete(values).duplicates(user => user.id);

      values   //?
      Eusers   //?
      deleted  //?
      Edeleted //?


      expect(values).toEqual(Eusers);
      expect(deleted).toEqual(Edeleted);
   });


   it(`Empty`, async () => {
      const Edeleted = [];
      const Eusers = [];

      const values_1 = [];
      const values_2 = [users[0]];
      const result_1 = _delete(values_1).match(user => user.id);
      const result_2 = _delete([...values_2]).index([]);

      result_1 //?
      result_2 //?
      Eusers   //?
      Edeleted //?

      expect(values_1).toEqual(Eusers);
      expect(result_1).toEqual(Edeleted);
      expect(result_2).toEqual(Edeleted);
      expect(values_2).toEqual(values_2);
   });
});