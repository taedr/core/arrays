import { _update, IUpdate, IChange, updateByKey } from '@taedr/arrays';
import { FN_PACK } from '@taedr/utils';


describe(`Update`, () => {
   class User {
      constructor(
         readonly id: string,
         readonly age: number
      ) { }
   }

   const users = [
      new User(`Olga`, 12),
      new User(`Evdokia`, 32),
      new User(`Elena`, 56),
      new User(`Ulia`, 91),
      new User(`Tatiana`, 14),
      new User(`Viktoria`, 22),
      new User(null, 22),
      new User(`Galina`, 33),
   ];

   const by = (user: User) => user.id;

   it(`Compare`, async () => {
      const values = [...users];
      const updates = [
         new User(`Olga`, 1),
         new User(`Galina`, 2),
      ];

      const Evalues = [updates[0], ...users.slice(1, -1), updates[1]];
      const EnotMatched: User[] = [];
      const Eupdated: IUpdate<User>[] = [
         { index: 0, value: updates[0], prev: users[0] },
         { index: 7, value: updates[1], prev: users[7] },
      ];

      const { notMatched, changes } = _update(values).keys(by, ...updates);

      values      //?
      Evalues     //?
      changes     //?
      Eupdated    //?
      notMatched  //?
      EnotMatched //?

      expect(values).toEqual(Evalues);
      expect(changes).toEqual(Eupdated);
      expect(notMatched).toEqual(EnotMatched);
   });


   it(`Should use latest entries if 'by' function returns same result.
   If value returned by 'by' doens't correlate with any entry in origin array element or
   is null / undefined it should be placed into 'notMatched'.
   If 'by' on origin elemnet returns null / undefined it should not be considered updatable.`, async () => {
      const values = [...users];
      const updates = [
         new User(`Stepashka`, 43),
         new User(`Olga`, 1),
         new User(`Galina`, 2),
         new User(`Jenek`, 17),
         new User(`Olga`, 10),
         new User(`Galina`, 7),
         new User(null, 7),
         new User(`Galina`, 20),
         new User(`Vasek`, 32),
      ];

      const Evalues = [updates[4], ...users.slice(1, -1), updates[7]];
      const EnotMatched: User[] = [6, 0, 3, 8].map(index => updates[index]);
      const Eupdated: IUpdate<User>[] = [
         { index: 0, value: updates[4], prev: users[0] },
         { index: 7, value: updates[7], prev: users[7] },
      ];

      const { notMatched, changes } = _update(values).keys(by, ...updates);

      values      //?
      Evalues     //?
      changes     //?
      Eupdated    //?
      notMatched  //?
      EnotMatched //?

      expect(values).toEqual(Evalues);
      expect(changes).toEqual(Eupdated);
      expect(notMatched).toEqual(EnotMatched);
   });


   it(`By index`, async () => {
      const values = [...users];
      const farIndex = new User(`Ibragim`, 2);
      const updates: IChange<User>[] = [
         { value: new User(`Olga`, 1), index: 0 },
         { value: new User(`Stepashka`, 2), index: 6 },
         { value: new User(`Galina`, 2), index: 7 },
         { value: farIndex, index: 12 },
      ];

      const Evalues = [updates[0].value, ...users.slice(1, -2), updates[1].value, updates[2].value];
      const EnotMatched: User[] = [farIndex];
      const Eupdated = updates.slice(0, -1).map(({ value, index }) => {
         return { index, value, prev: users[index] };
      });

      const { notMatched, changes } = _update(values).indexes(...updates);

      values      //?
      Evalues     //?
      changes     //?
      Eupdated    //?
      notMatched  //?
      EnotMatched //?


      expect(values).toEqual(Evalues);
      expect(changes).toEqual(Eupdated);
      expect(notMatched).toEqual(EnotMatched);
   });


   it(`Duplicates`, async () => {
      const values = [...users];

      const Evalues = values;
      const Eupdated = values.map((value, index) => {
         if (by(value) === null) return;
         return { index, value, prev: users[index] };
      }).filter(Boolean);
      const EnotMatched = [values[6]];

      const { notMatched, changes } = _update(values).keys(by, ...values);

      values      //?
      Evalues     //?
      changes     //?
      Eupdated    //?
      notMatched  //?
      EnotMatched //?

      expect(values).toEqual(Evalues);
      expect(changes).toEqual(Eupdated);
      expect(notMatched).toEqual(EnotMatched);
   });


   it(`Empty elements in update array`, async () => {
      class User {
         constructor(
            public id: number
         ) { }
      }

      const users = FN_PACK(10).map(id => new User(id));
      const toUpdate = FN_PACK(3).map(id => new User(id))
      const updates = [, , ...toUpdate, ,];

      const EnotMatched = [];
      const Eupdated = [0, 1, 2].map(index => ({
         value: toUpdate[index],
         prev: users[index],
         index
      }))

      const { notMatched, changes } = updateByKey(users, updates, ({ id }) => id);

      changes //?

      expect(changes).toEqual(Eupdated);
      expect(notMatched).toEqual(EnotMatched);
   });


   it(`By map`, async () => {
      const values = [...users];
      const farIndex = new User(`Ibragim`, 2);
      const updates: IChange<User>[] = [
         { value: new User(`Vasek`, 32), index: 0 },
         { value: new User(`Stepashka`, 2), index: 6 },
         { value: new User(`Galina`, 2), index: 7 },
         { value: farIndex, index: 12 },
      ];

      const Evalues = [updates[0].value, ...users.slice(1, -2), updates[1].value, updates[2].value];
      const EnotMatched: User[] = [farIndex];
      const Eupdated = updates.slice(0, -1).map(({ value, index }) => {
         return { index, value, prev: users[index] };
      });

      const { notMatched, changes } = _update(values).indexes(...updates);

      values      //?
      Evalues     //?
      changes     //?
      Eupdated    //?
      notMatched  //?
      EnotMatched //?


      expect(values).toEqual(Evalues);
      expect(changes).toEqual(Eupdated);
      expect(notMatched).toEqual(EnotMatched);
   });
});
