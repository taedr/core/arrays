import { _insert, IChange } from '@taedr/arrays';



describe(_insert.name, () => {
   class User {
      constructor(
         readonly id: string
      ) { }
   }

   const users = [
      new User(`Olga`),
      new User(`Evdokia`),
      new User(`Elena`),
      new User(`Ulia`),
      new User(`Tatiana`),
      new User(`Viktoria`),
      new User(`Galina`),
   ];



   it(`TAIL`, async () => {
      const values = users.slice(0, 4);
      const toInsert = users.slice(4);

      const Evalues = [...values, ...toInsert];
      const Einserted = toInsert.map((value, i) => ({ value, index: i + values.length }));

      const inserted = _insert(values).tail(...toInsert)

      toInsert  //?
      values    //?
      inserted  //?
      Einserted //?

      expect(values).toEqual(Evalues);
      expect(inserted).toEqual(Einserted);
   });


   it(`HEAD`, async () => {
      const values = users.slice(0, 4);
      const toInsert = users.slice(4);

      const Evalues = [...toInsert, ...values];
      const Einserted = toInsert.map((value, index) => ({ value, index }));

      const inserted = _insert(values).head(...toInsert)

      values    //?
      inserted  //?
      Einserted //?

      expect(values).toEqual(Evalues);
      expect(inserted).toEqual(Einserted);
   });


   it(`Index`, async () => {
      const from = 2;
      const to = 5;
      const left = users.slice(0, from);
      const right = users.slice(to);
      const values = [...left, ...right];
      const toInsert = users.slice(from, to);

      const Evalues = [...left, ...toInsert, ...right];
      const Einserted = toInsert.map((value, i) => ({ value, index: i + from }));

      const inserted = _insert(values).index(from, ...toInsert)

      values    //?
      inserted  //?
      Einserted //?

      expect(values).toEqual(Evalues);
      expect(inserted).toEqual(Einserted);
   });


   it(`Indexes`, async () => {
      const from = 2;
      const to = 5;
      const left = users.slice(0, from);
      const right = users.slice(to);
      const values = [...left, ...right];
      const toInsert: IChange<User>[] = [
         { value: users[2], index: 0 },
         { value: users[3], index: 3 },
         { value: users[4], index: 1 },
      ];

      const Evalues = [2, 4, 0, 3, 1, 5, 6].map(index => users[index]);
      const Einserted = [...toInsert].sort((a, b) => a.index - b.index);

      const inserted = _insert(values).indexes(...toInsert);


      values      //?
      toInsert    //?
      inserted    //?
      Einserted   //?

      expect(values).toEqual(Evalues);
      expect(inserted).toEqual(Einserted);
   });


   it(`Empty`, async () => {
      const values_1 = [users[0]];
      const values_2 = [];
      const result_1 = _insert(values_1).tail();
      const result_2 = _insert(values_2).indexes();

      expect(result_1).toEqual([]);
      expect(result_2).toEqual([]);
   });
});