import { FILTER } from '@taedr/arrays';

describe(`Object`, () => {
   class User {
      constructor(
         readonly id: number,
         readonly address: Address,
      ) { }
   }

   class Address {
      constructor(
         readonly country: string,
      ) { }
   }

   const ukraine = new Address(`ukraine`);
   const spain = new Address(`spain`);

   const users = [
      new User(1, ukraine),
      new User(2, spain),
      new User(3, ukraine),
      new User(4, spain),
      new User(5, spain),
   ];

   const byAddress = ({ address }: User) => address;

   it(`=`, () => {
      const Evalues = users.filter(user => user.address === ukraine);

      const filtred = FILTER.object.tag({
         value: ukraine,
      }, users, byAddress)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });

   it(`!=`, () => {
      const Evalues = users.filter(user => user.address !== ukraine);

      const filtred = FILTER.object.tag({
         result: 'EXCLUDE',
         value: ukraine,
      }, users, byAddress)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });

   it(`=`, () => {
      const Evalues = users.filter(user => user.address === spain);

      const filtred = FILTER.object.tag({
         result: 'EXCLUDE',
         value: ukraine,
      }, users, byAddress)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });

});