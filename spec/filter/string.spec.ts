import { FILTER } from '@taedr/arrays';

describe(`String`, () => {
   class User {
      constructor(
         readonly id: number,
         readonly dob: string,
         readonly country: string,
      ) { }
   }

   const users = [
      new User(1, `02.06.1995`, `ukraine`),
      new User(2, `06.02.1997`, `russia`),
      new User(3, `03.03.1995`, `spain`),
      new User(4, `11.06.1995`, `ukraine`),
      new User(5, `04.03.1997`, `spain`),
      new User(6, `04.03.1997`, `ukraine`),
   ];

   const byDob = ({ dob }: User) => dob;
   const byCountry = ({ country }: User) => country;

   it(`INCLUDE`, () => {
      const Evalues = [...users]
      Evalues.splice(1, 1);

      const filtred = FILTER.string.tag({
         mode: 'CONTAINS',
         value: 'ai',
      }, users, byCountry)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });


   it(`EXCLUDE`, () => {
      const Evalues = [users[1]];

      const filtred = FILTER.string.tag({
         mode: 'CONTAINS',
         result: 'EXCLUDE',
         value: 'ai',
      }, users, byCountry)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });


   it(`EQUAL`, () => {
      const country = `ukraine`;
      const Evalues = users.filter(user => user.country === country);

      const filtred = FILTER.string.tag({
         mode: 'EQUALS',
         value: country,
      }, users, byCountry)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });


   it(`START`, () => {
      const start = `04`;
      const Evalues = users.filter(user => {
         return user.dob.slice(0, start.length) === start;
      });

      const filtred = FILTER.string.tag({
         mode: 'START',
         value: start,
      }, users, byDob)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });


   it(`END`, () => {
      const end = `97`;
      const Evalues = users.filter(user => {
         return user.dob.slice(-end.length) === end;
      });

      const filtred = FILTER.string.tag({
         mode: 'END',
         value: end,
      }, users, byDob)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });
});
