import { FILTER } from '@taedr/arrays';

describe(`Boolean`, () => {
   class User {
      constructor(
         readonly id: number,
         readonly isAlive: boolean,
      ) { }
   }

   const users = [
      new User(1, true),
      new User(2, false),
      new User(3, false),
      new User(4, true),
      new User(5, false),
      new User(6, true),
   ];

   const byIsAlive = ({ isAlive }: User) => isAlive;

   it(`true`, () => {
      const Evalues = users.filter(user => user.isAlive);

      const filtred = FILTER.boolean.tag({
         value: true,
      }, users, byIsAlive)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });

   it(`not true`, () => {
      const Evalues = users.filter(user => !user.isAlive);

      const filtred = FILTER.boolean.tag({
         result: 'EXCLUDE',
         value: true,
      }, users, byIsAlive)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });

   it(`false`, () => {
      const Evalues = users.filter(user => !user.isAlive);

      const filtred = FILTER.boolean.tag({
         value: false,
      }, users, byIsAlive)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });

   it(`not false`, () => {
      const Evalues = users.filter(user => !!user.isAlive);

      const filtred = FILTER.boolean.tag({
         result: 'EXCLUDE',
         value: false,
      }, users, byIsAlive)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });
});