import { FILTER } from '@taedr/arrays';

describe(`Number`, () => {
   class User {
      constructor(
         readonly id: number,
         readonly age: number,
      ) { }
   }

   const users = [
      new User(1, 10),
      new User(2, 12),
      new User(3, 21),
      new User(4, 43),
      new User(5, 9),
      new User(6, 12),
   ];

   const byAge = ({ age }: User) => age;
   const age = 12;

   it(`=`, () => {
      const Evalues = users.filter(user => user.age === age);

      const filtred = FILTER.number.tag({
         mode: '=',
         value: age,
      }, users, byAge)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });

   it(`!=`, () => {
      const Evalues = users.filter(user => user.age !== age);

      const filtred = FILTER.number.tag({
         mode: '=',
         result: 'EXCLUDE',
         value: age,
      }, users, byAge)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });

   it(`<`, () => {
      const Evalues = users.filter(user => user.age < age);

      const filtred = FILTER.number.tag({
         mode: '<',
         value: age,
      }, users, byAge)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });

   it(`>`, () => {
      const Evalues = users.filter(user => user.age > age);

      const filtred = FILTER.number.tag({
         mode: '>',
         value: age,
      }, users, byAge)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });

   it(`>=`, () => {
      const Evalues = users.filter(user => user.age >= age);

      const filtred = FILTER.number.tag({
         mode: '>=',
         value: age,
      }, users, byAge)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });

   it(`<=`, () => {
      const Evalues = users.filter(user => user.age <= age);

      const filtred = FILTER.number.tag({
         mode: '<=',
         value: age,
      }, users, byAge)

      filtred  //?
      Evalues  //?

      expect(filtred).toEqual(Evalues);
   });
});