import { TPack } from '@taedr/utils';
import { ISortable, _sort } from '@taedr/arrays';

class Person {
   constructor(
      readonly country: string,
      readonly age: number,
      readonly name: string,
   ) { }
}

const persons: Person[] = [
   new Person(`A`, 1, `D`),
   new Person(`B`, 1, `D`),
   new Person(`C`, 2, `F`),
   new Person(`A`, 2, `E`),
   new Person(`A`, 3, `E`),
   new Person(`B`, 2, `E`),
   new Person(`B`, 1, `E`),
   new Person(`C`, 2, `E`),
   new Person(`C`, 3, `F`),
   new Person(`A`, 2, `E`),
   new Person(`A`, 1, `E`),
   new Person(`A`, 3, `E`),
   new Person(`B`, 3, `E`),
   new Person(`B`, 2, `E`),
   new Person(`B`, 1, `F`),
   new Person(`B`, 3, `E`),
   new Person(`B`, 1, `E`),
   new Person(`C`, 1, `E`),
   new Person(`A`, 1, `E`),
   new Person(`A`, 1, `D`),
   new Person(`A`, 1, `F`),
   new Person(`A`, 1, `F`),
   new Person(`B`, 3, `E`),
];

it(`Asc`, () => {
   const chain: TPack<ISortable<Person>> = [
      { state: 'asc', by: user => user.country },
      { state: 'asc', by: user => user.age },
      { state: 'asc', by: user => user.name },
   ];

   const sorted = _sort(persons, chain)
   const result = [
      { country: 'A', age: 1, name: 'D' },
      { country: 'A', age: 1, name: 'D' },
      { country: 'A', age: 1, name: 'E' },
      { country: 'A', age: 1, name: 'E' },
      { country: 'A', age: 1, name: 'F' },
      { country: 'A', age: 1, name: 'F' },
      { country: 'A', age: 2, name: 'E' },
      { country: 'A', age: 2, name: 'E' },
      { country: 'A', age: 3, name: 'E' },
      { country: 'A', age: 3, name: 'E' },
      { country: 'B', age: 1, name: 'D' },
      { country: 'B', age: 1, name: 'E' },
      { country: 'B', age: 1, name: 'E' },
      { country: 'B', age: 1, name: 'F' },
      { country: 'B', age: 2, name: 'E' },
      { country: 'B', age: 2, name: 'E' },
      { country: 'B', age: 3, name: 'E' },
      { country: 'B', age: 3, name: 'E' },
      { country: 'B', age: 3, name: 'E' },
      { country: 'C', age: 1, name: 'E' },
      { country: 'C', age: 2, name: 'E' },
      { country: 'C', age: 2, name: 'F' },
      { country: 'C', age: 3, name: 'F' }
   ]

   console.log(sorted)

   expect(sorted).toEqual(result);
});


it(`Desc`, () => {
   const chain: TPack<ISortable<Person>> = [
      { state: 'desc', by: user => user.country },
      { state: 'desc', by: user => user.age },
      { state: 'desc', by: user => user.name },
   ];

   const sorted = _sort(persons, chain)
   const result = [
      { country: 'C', age: 3, name: 'F' },
      { country: 'C', age: 2, name: 'F' },
      { country: 'C', age: 2, name: 'E' },
      { country: 'C', age: 1, name: 'E' },
      { country: 'B', age: 3, name: 'E' },
      { country: 'B', age: 3, name: 'E' },
      { country: 'B', age: 3, name: 'E' },
      { country: 'B', age: 2, name: 'E' },
      { country: 'B', age: 2, name: 'E' },
      { country: 'B', age: 1, name: 'F' },
      { country: 'B', age: 1, name: 'E' },
      { country: 'B', age: 1, name: 'E' },
      { country: 'B', age: 1, name: 'D' },
      { country: 'A', age: 3, name: 'E' },
      { country: 'A', age: 3, name: 'E' },
      { country: 'A', age: 2, name: 'E' },
      { country: 'A', age: 2, name: 'E' },
      { country: 'A', age: 1, name: 'F' },
      { country: 'A', age: 1, name: 'F' },
      { country: 'A', age: 1, name: 'E' },
      { country: 'A', age: 1, name: 'E' },
      { country: 'A', age: 1, name: 'D' },
      { country: 'A', age: 1, name: 'D' }
   ];

   console.log(sorted)

   expect(sorted).toEqual(result);
});