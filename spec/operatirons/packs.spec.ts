import { sliceOnPacks } from '@taedr/arrays';

describe(`Packs`, () => {
   it(sliceOnPacks.name, () => {
      const values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
      const Evalues: number[][] = [];
      const packs = 3;

      let pack: number[] = [values[0]];
      for (let i = 1; i < values.length; i++) {
         if (i % packs === 0) {
            Evalues.push(pack);
            pack = [];
         }

         pack.push(values[i]);
      }
      if (pack.length > 0) Evalues.push(pack);

      const sliced = sliceOnPacks(values, packs);

      sliced   //?
      Evalues  //?

      expect(sliced).toEqual(Evalues);
   });
});
