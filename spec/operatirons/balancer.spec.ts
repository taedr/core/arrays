import { balancer } from '@taedr/arrays';
import { FN_MIRROR } from '@taedr/utils'


describe(`Balancer`, () => {
   it(balancer.name, () => {
      const arrayOld = new Array(200).fill(null).map((_, i) => i);
      const min = 17;
      const debounceTotal = arrayOld.reduce((acc, val) => acc += val, 0);

      balancer(arrayOld, min, FN_MIRROR, (val, i, newVal) => arrayOld[i] = newVal);
      
      arrayOld //?

      const balancedTotal = Math.round(arrayOld.reduce((acc, val) => acc += val, 0));
      const allBiggerORequalMin = arrayOld.every(val => val >= min);


      expect(balancedTotal).toBe(debounceTotal);
      expect(allBiggerORequalMin).toBeTruthy();
   });
});