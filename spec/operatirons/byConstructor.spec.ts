import { sliceByConstructor } from '@taedr/arrays';


describe(sliceByConstructor.name, () => {
   class A {
      constructor(
         public a: string,
      ) { }
   }
   
   
   class B extends A {
      constructor(
         public a: string,
         public b: number,
      ) {
         super(a);
      }
   }
   
   
   class C extends B {
      constructor(
         public a: string,
         public b: number,
         public c: boolean,
      ) {
         super(a, b);
      }
   }


   it(`By Constructor`, () => {
      const users = [
         new A('Gala'),
         new B('Ibragim', 90),
         new C('Aphanasii', 105, false),
         new A('Hulio'),
         new C('Roza', 17, true),
      ];

      const sliced = sliceByConstructor(users, [A, B, C]);

      sliced //?

      expect(sliced.get(A)[0]).toBe(users[0]);
      expect(sliced.get(A)[1]).toBe(users[3]);
      expect(sliced.get(B)[0]).toBe(users[1]);
      expect(sliced.get(C)[0]).toBe(users[2]);
      expect(sliced.get(C)[1]).toBe(users[4]);
   });
});


