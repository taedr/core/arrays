import { correlation, difference, correlationWeak, differenceWeak, deduct, combine } from '@taedr/arrays';
import { FN_MIRROR } from '@taedr/utils';



describe(`Compare`, () => {
   const arr_1 = [1, 2, 3, 4, 5, 6, 7, 8, 12, 67, 45];
   const arr_2 = [2, 8, 9, 11, 5, 6];
   const arr_3 = [32, 11, 5, 8, 11, 6, 12];
   const arr_4 = [15, 17, 8, 19, 7, 3, 6, 11];
   const arr_5 = [7, 8, 32, 15, 104, 3, 6];
   const pack = [arr_1, arr_2, arr_3, arr_4, arr_5];

   it(correlation.name, () => {
      const Ecorrelation = [6, 8];
      const Rcorrelation = correlation(pack, FN_MIRROR);

      Rcorrelation   //?
      Ecorrelation   //?

      expect(Rcorrelation).toEqual(Ecorrelation);
   });


   it(correlationWeak.name, () => {
      const Ecorrelation = [2, 3, 5, 6, 7, 8, 12, 11, 32, 15];
      const Rcorrelation = correlationWeak(pack, FN_MIRROR);

      Rcorrelation   //?
      Ecorrelation   //?

      expect(Rcorrelation).toEqual(Ecorrelation);
   });


   it(difference.name, () => {
      const Edifference = [1, 4, 67, 45, 9, 17, 19, 104];
      const Rdifference = difference(pack, FN_MIRROR);

      Edifference   //?
      Rdifference   //?

      expect(Rdifference).toEqual(Edifference);
   });


   it(differenceWeak.name, () => {
      const Edifference = [1, 2, 3, 4, 5, 7, 12, 67, 45, 9, 11, 32, 15, 17, 19, 104];
      const Rdifference = differenceWeak(pack, FN_MIRROR);

      Edifference   //?
      Rdifference   //?

      expect(Rdifference).toEqual(Edifference);
   });


   it(deduct.name, () => {
      const [root, ...tail] = pack;
      const Ededuct = [1, 4, 67, 45];
      const Rdeduct = deduct(root, tail, FN_MIRROR);

      Ededuct   //?
      Rdeduct   //?

      expect(Rdeduct).toEqual(Ededuct);
   });


   it(combine.name, () => {
      const Ecombine = [ 1, 2, 3, 4, 5, 6, 7, 8, 12, 67, 45, 9, 11, 32, 15, 17, 19, 104 ];
      const Rcombine = combine(pack, FN_MIRROR);

      Ecombine   //?
      Rcombine   //?

      expect(Rcombine).toEqual(Ecombine);
   });
});