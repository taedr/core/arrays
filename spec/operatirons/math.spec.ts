import { min, max, std, avg } from '@taedr/arrays';


describe(`Math`, () => {
   class User {
      constructor(
         readonly age: number,
      ) { }
   }

   const users = [5, 4, 8, 9, 11, 2, 5, -10, 3, 21, 0].map(age => new User(age));
   const by = (user: User) => user.age;

   it(min.name, async () => {
      const Emin = { value: { age: -10 }, min: -10, index: 7 };
      const Rmin = min(users, by);

      Emin  //?
      Rmin  //?

      expect(Rmin).toEqual(Emin);
   });


   it(max.name, async () => {
      const Emax = { value: { age: 21 }, max: 21, index: 9 };
      const Rmax = max(users, by);

      Emax  //?
      Rmax  //?

      expect(Rmax).toEqual(Emax);
   });


   it(avg.name, async () => {
      const Eavg = users.reduce((acc, { age }) => acc + age, 0) / users.length;
      const Ravg = avg(users, by);

      Eavg  //?
      Ravg  //?

      expect(Ravg).toEqual(Eavg);
   });


   it(std.name, async () => { // HZ
      const Estd = 7.6169667071729945; 
      const Rstd = std(users, by);

      Estd  //?
      Rstd  //?

      expect(Rstd).toEqual(Estd);
   });
});